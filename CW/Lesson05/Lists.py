if __name__ == '__main__':
    count = int(input("Write the count of numbers - "))
    lst_one = []
    for i in range(count):
        lst_one.append(int(input("Write the number - ")))
    print(lst_one)

    count = int(input("Write the count of numbers - "))
    lst_two = []
    for i in range(count):
        lst_two.append(int(input("Write the number - ")))
    print(lst_two)

    lst_one.append(99)
    print(lst_one)
    tuple_one = tuple(lst_one)
    print(tuple_one)
    print(tuple_one[0])

    set_one = set(lst_one)
    set_two = set(lst_two)
    print(set_one)
    print(set_one)
    print(set_one.issubset(set_two))
    print(set_one.issuperset(set_two))
    print(set_one.discard(2))
    print(set_one.remove(2))
