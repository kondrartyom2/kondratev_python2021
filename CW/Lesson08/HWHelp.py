def how_match_upper_letter(string: str):
    sum = 0
    for elem in string.split(" "):
        if 'A' <= elem[0] <= 'Z':
            sum += 1
    return sum


if __name__ == '__main__':
    string = input("Введите строку - ")
    print(how_match_upper_letter(string))
