class RationalFraction:

    def __init__(self, numerator: int = 1, denominator: int = 1):
        if denominator == 0:
            raise Exception
        self.numerator = numerator
        self.denominator = denominator

    def reduce(self):
        limit = min(self.numerator, self.denominator)
        for i in reversed(range(2, limit + 1)):
            if (self.numerator % i == 0) and (self.denominator % i == 0):
                self.numerator = int(self.numerator / i)
                self.denominator = int(self.denominator / i)

    def __sub__(self, other):
        answer = RationalFraction(denominator=(self.denominator * other.denominator),
                                  numerator=((self.numerator * other.denominator) - (other.numerator * self.denominator)))
        answer.reduce()
        return answer

    def __isub__(self, other):
        self.numerator = (self.numerator * other.denominator) - (other.numerator * self.denominator)
        self.denominator = self.denominator * other.denominator
        self.reduce()

    def __add__(self, other):
        answer = RationalFraction(denominator=(self.denominator * other.denominator),
                                  numerator=((self.numerator * other.denominator) + (other.numerator * self.denominator)))
        answer.reduce()
        return answer

    def __iadd__(self, other):
        self.numerator = (self.numerator * other.denominator) + (other.numerator * self.denominator)
        self.denominator = self.denominator * other.denominator
        self.reduce()

    def __mul__(self, other):
        answer = RationalFraction(denominator=(self.denominator * other.denominator),
                                  numerator=(self.numerator * other.numerator))
        answer.reduce()
        return answer

    def __imul__(self, other):
        self.denominator = (self.denominator * other.denominator)
        self.numerator = self.numerator * other.numerator
        self.reduce()

    def __div__(self, other):
        answer = RationalFraction(denominator=(self.denominator * other.numerator),
                                  numerator=(self.numerator * other.denominator))
        answer.reduce()
        return answer

    def __idiv__(self, other):
        self.denominator = self.denominator * other.numerator
        self.numerator = self.numerator * other.denominator
        self.reduce()

    def __str__(self):
        return "{}/{}".format(self.numerator, self.denominator)

    def __eq__(self, other):
        return self.numerator == other.numerator and self.denominator == other.denominator

    def value(self):
        return self.numerator / self.denominator

    def number_part(self):
        return int(self.value())