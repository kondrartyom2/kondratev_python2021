from CW.Lesson09.RationalFraction import RationalFraction
from CW.Lesson09.Vector2d import Vector2d

if __name__ == '__main__':
    rf_one = RationalFraction(1, 2)
    rf_two = RationalFraction(3, 4)
    rf_three = RationalFraction(3, 9)
    rf_four = RationalFraction(5, 6)
    vector_one = Vector2d(int(input("Write coordinate x of first vector - ")),
                          int(input("Write coordinate y of first vector - ")))
    vector_two = Vector2d(int(input("Write coordinate x of second vector - ")),
                          int(input("Write coordinate y of second vector - ")))
    vector_three = Vector2d(rf_one, rf_two)
    vector_four = Vector2d(rf_three, rf_four)
    print(vector_one)
    print(vector_two)
    print(vector_three)
    print(vector_four)
    print(vector_one + vector_two)
    print(vector_three + vector_four)
    print(vector_one == vector_two)
    print(vector_three == vector_four)
    print(vector_one == vector_four)



