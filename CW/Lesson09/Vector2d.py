class Vector2d:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, vector):
        return Vector2d(x=(self.x + vector.x), y=(self.y + vector.y))

    def add(self, vector):
        self.x += vector.x
        self.y += vector.y

    def __sub__(self, vector):
        return Vector2d(x=(self.x - vector.x), y=(self.y - vector.y))

    def sub(self, vector):
        self.x -= vector.x
        self.y -= vector.y

    def __mul__(self, vector):
        return Vector2d(x=(self.x * vector.x), y=(self.y * vector.y))

    def mult(self, vector):
        self.x *= vector.x
        self.y *= vector.y

    def __str__(self):
        return "Vector(x = {}, y = {})".format(self.x, self.y)

    def __len__(self):
        return abs(self.x - self.y)

    def scalar_product(self, vector):
        return self.__len__() * len(vector)

    def __eq__(self, vector):
        return self.x == vector.x and self.y == vector.y




