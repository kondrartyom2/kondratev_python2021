class User:

    def __init__(self, username: str = "user"):
        self.username = username
        self.hp = 100

    def __isub__(self, power: int = 0):
        self.hp -= power

    def __str__(self):
        return "{}: hp = {}".format(self.username, self.hp)

    def is_alive(self):
        return self.hp > 0