import random
from random import Random

from CW.Lesson11.User import User

percents = {1: 100, 2: 90, 3: 80, 4: 70, 5: 60, 6: 50, 7: 40, 8: 30, 9: 20}


def get_chance(percent: int) -> bool:
    percent /= 10
    chance = random.randint(1, 9)
    return 0 < chance < percent


def validate_kick_power(power: int) -> int:
    return power if get_chance(percents.get(power)) else 0


def kick(user_one: User, user_two: User) -> bool:
    power = int(input("{} введи силу удара - ".format(user_one.username)))
    while not 0 < power < 10:
        power = int(input("{} введи еще раз силу удара - ".format(user_one.username)))
    power = validate_kick_power(power)
    if power == 0:
        return False
    else:
        user_two -= power * 10
        return True


if __name__ == '__main__':
    print("Игра началась")
    user_one = User(input("Введите username первого игрока - "))
    user_two = User(input("Введите username второго игрока - "))
    who_is_first = random.randint(1, 2)
    if who_is_first == 2:
        user_one, user_two = user_two, user_one
    flag = True
    while flag:
        if user_two.is_alive():
            if kick(user_one, user_two):
                print("{} ты попал".format(user_one.username))
                print(user_two)
            else:
                print("{} ты промахнулся".format(user_one.username))
                print(user_two)
            user_one, user_two = user_two, user_one
        else:
            print("{} ты выиграл".format(user_one))
            flag = False
