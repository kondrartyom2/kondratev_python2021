if __name__ == '__main__':
    # array = list(range(10))
    # for elem in array:
    #     print(elem)
    #
    # array = [1, 3, 4]
    # for elem in array:
    #     print(elem)
    #
    # for i in range(len(array)):
    #     array[i] += 1
    num = int(input("Введите число - "))
    sum = 0

    while num > 0:
        sum += int(num % 10)
        num = int(num / 10)
    print("Ответ - " + str(sum))