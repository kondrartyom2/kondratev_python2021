from random import randrange


class Player:

    def __init__(self, username: str):
        self.username = username
        self.hp = 100

    def __str__(self):
        return "У игрока {} осталоcь hp - {}".format(self.username, self.hp)

    def is_alive(self):
        return self.hp > 0

    def kicked(self, power: int):
        if 0 < power < 10:
            random = randrange(10)
            if power == 1:
                self.hp -= 10
            elif power == 2 and random < 9:
                self.hp -= 20
            elif power == 3 and random < 8:
                self.hp -= 30
            elif power == 4 and random < 7:
                self.hp -= 40
            elif power == 5 and random < 6:
                self.hp -= 50
            elif power == 6 and random < 5:
                self.hp -= 60
            elif power == 7 and random < 4:
                self.hp -= 70
            elif power == 8 and random < 3:
                self.hp -= 80
            elif power == 9 and random < 2:
                self.hp -= 90