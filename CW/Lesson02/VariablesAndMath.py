if __name__ == '__main__':
    num_1 = 4
    print(type(num_1))
    num_2 = 6
    print(type(num_2))
    print(num_1 + num_2)
    print(num_1 - num_2)
    print(num_1 << num_2)
    print(num_1 >> num_2)
    print(num_1 / num_2)
    print(num_1 * num_2)
    print(num_1 % num_2)

    num_3 = 5
    num_3 += 1
    print(num_3)
    num_3 -= 1
    print(num_3)
    num_3 *= 1
    print(num_3)
    num_3 >>= 1
    print(num_3)
    num_3 <<= 1
    print(num_3)
    num_3 /= 1
    print(num_3)
    num_3 %= 1
    print(num_3)
