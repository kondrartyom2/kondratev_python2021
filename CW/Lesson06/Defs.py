def get_arr_by_count(count: int) -> list:
    lst = []
    for i in range(count):
        lst.append(int(input("Введите число - ")))
    return lst


def get_arr() -> list:
    return get_arr_by_count(int(input("Введите кол-во элементов списка - ")))


def min_n_max_of_list(lst: list) -> (int, int):
    if len(lst) > 0:
        min = lst[0]
        max = lst[0]
        for elem in lst:
            min = elem if elem < min else min
            max = elem if elem > max else max
        return min, max
    else:
        raise RuntimeError


def all_elements_is_even(lst: list) -> bool:
    for elem in lst:
        if elem % 2 > 0:
            return False
    return True


if __name__ == '__main__':
    # Я хочу ввести число(кол-во элем. списка)
    # Я хочу ввести числа для этого массива
    lst = get_arr()
    # Я хочу найти мин и макс этого списка
    print(min_n_max_of_list(lst))
    # Я хочу понять все ли числа в нем четны
    print(all_elements_is_even(lst))
    lst = get_arr()