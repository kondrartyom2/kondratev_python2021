from typing import List


def get_list() -> List[int]:
    count = int(input("Введите кол-во элементов - "))
    lst = []
    for i in range(count):
        lst.append(int(input("Введите элемент - ")))
    return lst


def sum_of_list(lst: List[int]) -> int:
    sum = 0
    for elem in lst:
        sum += elem
    return sum


def get_min_max_of_list(lst: List[int]) -> (int, int):
    if len(lst) > 0:
        min_of_list = lst[0]
        max_of_list = lst[0]
        for elem in lst:
            min_of_list = elem if elem < min_of_list else min_of_list
            max_of_list = elem if elem > max_of_list else max_of_list
        return min_of_list, max_of_list
    raise IndexError


def factorial(number: int) -> int:
    if number > 1:
        return number * factorial(number - 1)
    return number


def get_factorials_set_of_list(lst: List[int]) -> set:
    facts = set()
    for elem in lst:
        facts.add(factorial(elem))
    return facts


def sort(lst: List[int]) -> List[int]:
    for i in range(len(lst)):
        for j in range(1, len(lst) - i):
            if lst[j] < lst[j - 1]:
                elem = lst[j - 1]
                lst[j - 1] = lst[j]
                lst[j] = elem
    return lst


if __name__ == '__main__':
    lst = get_list()
    print(sum_of_list(lst))
    print(sort(lst))
    # print(get_min_max_of_list(lst))
