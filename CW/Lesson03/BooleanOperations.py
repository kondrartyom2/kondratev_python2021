# if __name__ == '__main__':
    # num = int(input("Введите число от 0 до 9 - "))
    # print("Число лежит в промежутке от 2 до 6")
    # answer_one = (num > 2) and (num < 6)
    # print(answer_one)
    # print("Да" if answer_one else "Нет")
    # print("Четно ли оно?")
    # answer_two = (num % 2) == 0
    # print("Да" if answer_two else "Нет")
    # print(num * 2)
    # if num > 4:
    #     print("Yessss")
    # else:
    #     print("Noooooo")


if __name__ == '__main__':
    # num = int(input("Введите число от 0 до 20 - "))
    # if num < 0 or num > 20:
    #     print("error")
    # else:
    #     if num > 9:
    #         print("Вторая десятка")
    #     else:
    #         if num == 0:
    #             print("Null")
    #         elif num == 1:
    #             print("One")
    #         elif num == 2:
    #             print("Two")
    #         elif num == 3:
    #             print("Three")
    x = int(input("Введите x - "))
    y = ((x**2 - 1) / (x + 2)) if x > 2 else (
        ((x**2 - 1) * (x + 2)) if x > 0 and x <= 2
        else (x**2 * (1 + 2 * x))
    )
    print(y)