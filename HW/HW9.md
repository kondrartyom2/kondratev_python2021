
1) Класс Vector2D - двумерный вектор. Атрибуты - два вещественных числа (координаты). Далее (здесь и в последующих подобных задачах) указываю методы с типом возвращаемых значений, а в скобках пишу только типы параметров. get- и set- методы создавать по необходимости (тоже здесь и далее). 
   2) Vector2D(int, int) - конструктор вектора с координатами; в конструкторах устраняйте дублирование кода; 
   3) __add__(Vector2d) - сложение вектора с другим вектором, результат возвращается как новый объект. 
   4) add(Vector2D) - сложение вектора с другим вектором, результат сохраняется в том, у кого был вызван этот метод; 
   5) __sub__(Vector2D) - вычитание из вектора другого вектора, результат возвращается как новый объект; 
   6) sub(Vector2D) - вычитание из вектора другого вектора, результат сохраняется в том векторе, у кого был вызван этот метод; 
   7) __mul__(double) - умножение вектора на вещественное число, результат возвращается как новый объект; 
   8) mul(double) - умножение вектора на вещественное число, результат сохраняется в векторе; 
   9) __str__() - строковое представление вектора;
   10) __len__() - длина вектора
   11) scalar_product(Vector2D) - скалярное произведение вектора на другой вектор;
   12) cos(Vector2D) - косинус угла между этим и другим вектором; 
   13) __eq__(Vector2D) - сравнить вектор с другим вектором;
2) Класс RationalFraction - рациональная дробь. Атрибуты - два целых числа (числитель и знаменатель).
   1) RationalFraction(int, int) - конструктор дроби со значениями числителя и знаменателя; в конструкторах устраняйте дублирование кода; 
   2) reduce() - сокращение дроби; 
   3) __add__(RationalFraction) - сложение дроби с другой дробью, результат возвращается как новый объект (не забудьте сократить)
   4) add(RationalFraction) - сложение дроби с другой дробью, результат сохраняется в том, у кого был вызван этот метод (не забудьте сократить); 
   5) __sub__(RationalFraction) - вычитание из дроби другой дроби, результат возвращается как новый объект (не забудьте сократить);
   6) sub(RationalFraction) - вычитание из дроби другой дроби, результат сохраняется в том, у кого был вызван этот метод (не забудьте сократить);
   7) __mul__(RationalFraction) - умножение дроби на другую дробь, результат возвращается как новый объект (сократить)
   8) mul(RationalFraction) - умножение дроби на другую дробь, результат сохраняется;
   9) __div__(RationalFraction) - деление дроби на другую дробь, результат возвращается как новый объект (сократить)
   10) div(RationalFraction) - деление дроби на другую дробь, результат сохраняется; больше не буду писать "возвращается" или "сохраняется", думаю, уже и так понятно.
   11) __str__() - строковое представление дроби (например, -2/3);
   12) value() - десятичное значение дроби;
   13) __eq__(RationalFraction) - сравнить дробь с другой дробью(не забывайте, что 2/4 и 1/2 - одна и та же дробь)
   14) number_part() - целая часть дроби;
